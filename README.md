# vue-cli 3x版本模板

#### 项目介绍
vue-cli 3x版本的初始化模板，包含框架cdn引入(由于[unpkg.com](https://unpkg.com)不太稳定，所以把框架放在本地，减少打包体量)

#### 安装步骤
``` bash
#下载必要的包(vue、webpack)
npm install

#启动测试服务，端口webpack自动分配，默认为8080
npm run dev

#启动打包服务
npm run build
```

#### 相关问题
如遇vue-cli相关问题，请参考[vue-cli文档](https://cli.vuejs.org/zh/guide/)