import App from './App.vue'
import routes from './router'
Vue.use(VueRouter)

Vue.config.productionTip = false;
Vue.config.devtools = true;

const router = new VueRouter({
    mode: "history",
    routes
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
