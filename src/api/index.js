var ajax = axios.create({
    baseURL: process.env.VUE_APP_API_URL, //接口地址，.env/.env.development文件进行修改
    headers: {}, //请求头
    withCredentials: true, //cookie
    crossDomain: true //跨域
})

//添加一个请求拦截器
ajax.interceptors.request.use(function(config) {
    //在请求发出之前进行一些操作
    var token = localStorage.getItem('token');
    if(token){
        config.headers['TOKEN'] = token
    }
    return config;
}, function(err) {
    console.error(err);
    ELEMENT.Message.error({
        message: '请求发生错误,请稍后再试!',
        type: 'error'
    });
    //Do something with request error
    return Promise.reject(err);
});
//添加一个响应拦截器
ajax.interceptors.response.use(function(res) {
    var status = res.data.status;
    if(!status){
        ELEMENT.Message.error({
            message: res.data.message,
            type: 'error'
        });
        return Promise.reject(res.data.message)
    }
    return (res.data.data ? res.data.data : res.data.status);
}, function(err) {
    console.error(err);
    ELEMENT.Message.error({
        message: '响应发生错误,请稍后再试!',
        type: 'error'
    });
    //Do something with response error
    return Promise.reject(err);
})
